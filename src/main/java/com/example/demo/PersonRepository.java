package com.example.demo;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PersonRepository extends Mapper<Person> {
    List<Person> getPersonInformation();
}