package com.example.demo;

import java.util.List;

public interface PersonService {
    List<Person> findAll();
}